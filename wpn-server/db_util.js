'use strict';
const assert = require('assert');

const module_map = new WeakMap();

module.exports = function(pg_promise) {
	// this allows passing in a pg-promise Database object instead of an
	// initialized pg-promise library
	if (pg_promise.$config && pg_promise.$config.pgp)
		pg_promise = pg_promise.$config.pgp;

	if (module_map.has(pg_promise))
		return module_map.get(pg_promise);

	const ret = {};

	// BEGIN ISOLATION LEVEL SERIALIZABLE, READ ONLY, DEFERRABLE;
	const tmSerializableRODeferrable = new pg_promise.txMode.TransactionMode({
		tiLevel: pg_promise.txMode.isolationLevel.serializable,
		readOnly: true,
		deferrable: true
	});
	ret.tmSerializableRODeferrable = tmSerializableRODeferrable;
	// BEGIN ISOLATION LEVEL SERIALIZABLE, READ WRITE;
	const tmSerializableRW = new pg_promise.txMode.TransactionMode({
		tiLevel: pg_promise.txMode.isolationLevel.serializable,
		readOnly: false
	});
	ret.tmSerializableRW = tmSerializableRW;

	// this is a helper to retry a transaction on serialization failure
	const transactor = async function(db, f) {
		assert(f.txMode, 'no txMode on f');
		assert(f.txMode instanceof pg_promise.txMode.TransactionMode, 'txMode wrong type');
		let error;
		for (let i = 0; i < 3; i++) {
			try {
				return await db.tx(f);
			} catch(e) {
				error = e;
				// 40001 is a serialization error
				// 40P01 is deadlock_detected
				if (e instanceof Error && (e.code === '40001' || e.code === '40P01'))
					;
				else
					break;
			}
		}
		throw error;
	};
	ret.transactor = transactor;

	// some nice wrappers around transactor follow
	// NOTE: instead of modifying the argument (which is always a bad idea)
	//       we create a new function to hold onto txMode.
	ret.transactorSROD = async function(db, f) {
		const g = async t => await f(t);
		g.txMode = tmSerializableRODeferrable;
		return await transactor(db, g);
	};

	ret.transactorSRW = async function(db, f) {
		const g = async t => await f(t);
		g.txMode = tmSerializableRW;
		return await transactor(db, g);
	};

	Object.freeze(ret);
	module_map.set(pg_promise, ret);
	return ret;
};
