CREATE TABLE networks (
	network_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
	name TEXT UNIQUE NOT NULL
);

CREATE FUNCTION networks_notify() RETURNS trigger AS $$
BEGIN
	IF TG_OP = 'DELETE' THEN
		PERFORM pg_notify('network_change', json_build_object('network_id', OLD.network_id)::text);
	ELSE
		PERFORM pg_notify('network_change', json_build_object('network_id', NEW.network_id)::text);
	END IF;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER networks_notify AFTER INSERT OR UPDATE OR DELETE
	ON networks
	FOR EACH ROW EXECUTE PROCEDURE networks_notify();

CREATE TABLE network_tinc_servers (
	network_id UUID NOT NULL REFERENCES networks,
	hostname TEXT NOT NULL,
	port INT NOT NULL,
	static BOOL NOT NULL,
	rsa_public_key TEXT,
	PRIMARY KEY(hostname, port)
);

CREATE TABLE roles (
	role_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
	name TEXT NOT NULL,
	network_id UUID NOT NULL REFERENCES networks,
	UNIQUE(network_id, name)
);

CREATE TABLE role_ip4 (
	role_id UUID NOT NULL REFERENCES roles,
	ips ip4r NOT NULL,
	masklen INT2 NOT NULL,
	PRIMARY KEY(role_id, ips),
	CHECK(set_masklen(lower(ips)::cidr, masklen)::ip4r >>= ips),
	EXCLUDE USING gist((role_id::text) WITH =, ips WITH &&)
);

CREATE TABLE role_subnets (
	role_id UUID REFERENCES roles,
	subnet CIDR,
	PRIMARY KEY(role_id, subnet)
);

CREATE TABLE nodes (
	-- tinc network name is a function of this node_id
	node_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
	-- network_id is only needed for the unique constraint.
	-- TODO: force network_id to be correct! Be very careful here
	network_id UUID NOT NULL REFERENCES networks,
	role_id UUID NOT NULL REFERENCES roles,
	rsa_public_key TEXT NOT NULL,
	ip4_address ip4 NOT NULL,
	last_seen TIMESTAMPTZ DEFAULT now(),
	UNIQUE(network_id, ip4_address)
);

CREATE TABLE users (
	user_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
	username TEXT NOT NULL UNIQUE,
	bcrypt_password TEXT,
	totp_secret TEXT
);

CREATE TABLE user_has_role (
	user_id UUID NOT NULL REFERENCES users(user_id),
	role_id UUID NOT NULL REFERENCES roles(role_id),
	PRIMARY KEY(user_id, role_id)
);

CREATE TABLE user_has_hostname (
	user_id UUID NOT NULL REFERENCES users(user_id),
	hostname TEXT NOT NULL,
	PRIMARY KEY(user_id, hostname)
);
