'use strict';

const assert = require('assert');
const child_process = require('mz/child_process');
const forge = require('node-forge');
const fs = require('fs-extra');
const net = require('net');
const os = require('os');
const path = require('path');
const sleep = require('es7-sleep');
const uuid = require('uuid-1345');

const bindToAddress_ = Symbol('TincNode.bindToAddress_');
const configdir_ = Symbol('TincNode.configdif_');
const connectTo_ = Symbol('TincNode.connectTo_');
const deviceType_ = Symbol('TincNode.deviceType_');
const hosts_ = Symbol('TincNode.hosts_');
const ips_ = Symbol('TincNode.ips_');
const name_ = Symbol('TincNode.name_');
const netns_ = Symbol('TincNode.netns_');
const port_ = Symbol('TincNode.port_');
const privateKey_ = Symbol('TincNode.privatekey_');
const publicKey_ = Symbol('TincNode.publickey_');
const subnets_ = Symbol('TincNode.subnets_');
const tinc_ = Symbol('TincNode.tinc_');

// this little block here allows the tinc-up script to tell us when tinc is
// up and ready instead of us waiting an arbitrary time to do so. This means
// that tinc's interface is available and nothing else.
const server_map = new Map();
const server = net.createServer(c => {
	let string = '';
	c.on('end', () => {
		const call = string.trim();
		if (server_map.has(call)) {
			server_map.get(call)();
			server_map.delete(call);
		}
	});

	c.on('data', (s) => {
		string += s;
	});
});

server.listen(0, '127.0.0.54');

module.exports = Object.freeze(class {
	// port === null means we assign a port ourselves
	constructor({
		configdir = path.join(os.tmpdir(), 'wpn_' + uuid.v4()),
		connectTo = [],
		hosts = [],
		ips = [],
		name = uuid.v4().replace(/-/g, '_'),
		netns = null,
		port = null,
	} = {}) {
		this.configdir = configdir;
		this.connectTo = connectTo;
		this.deviceType = null;
		this.hosts = hosts;
		this.ips = ips;
		this.name = name;
		this.netns = netns;
		this.port = port;
		this.subnets = [];
		this[bindToAddress_] = [];
		this[connectTo_] = [];
		this[privateKey_] = null;
		this[publicKey_] = null;
		this[tinc_] = null;
		Object.seal(this);
	}

	async generateKeyPair(bits = 2048) {
		const kp = await new Promise((resolve, reject) =>
			forge.pki.rsa.generateKeyPair({bits: bits, workers: -1}, (e, kp) =>
				e ? reject(e) : resolve(kp)
			)
		);
		this[privateKey_] = forge.pki.privateKeyToPem(kp.privateKey);
		this[publicKey_] = forge.pki.publicKeyToPem(kp.publicKey);
	}

	async start() {
		assert(this[privateKey_]);
		const signal = uuid.v4();
		const startup_wait = new Promise(resolve => server_map.set(signal, resolve));
		await this._writefiles(signal);
		let args = [];
		args = args.concat(
			['-d5', '-D', '-c', this.configdir, '--pidfile',
			path.join(this.configdir, 'wpn_tinc.pid')]
		);
		this[tinc_] = child_process.spawn('/usr/sbin/tincd', args);
		await startup_wait;
	}

	async stop() {
		if (this[tinc_]) {
			this[tinc_].kill();
			this[tinc_] = null;
			await fs.remove(this.configdir);
			await sleep(100);
		}
	}

	async reload() {
		if (this[tinc_]) {
			await this._writefiles('');
			// TODO: will not work on windows, will have to restart
			this[tinc_].kill('SIGHUP');
		}
	}

	set bindToAddress(addresses) {
		this[bindToAddress_] = addresses;
	}

	get bindToAddress() { return this[bindToAddress_]; }

	set configdir(c) {
		assert(typeof c === 'string');
		this[configdir_] = c;
		// schedule restart
	}

	get configdir() {
		return this[configdir_];
	}

	set connectTo(c) {
		this[connectTo_] = c;
	}

	get connectTo() { return this[connectTo_]; }

	set deviceType(t) {
		assert(t === null || t === 'dummy');
		this[deviceType_] = t;
	}

	get deviceType() { return this[deviceType_]; }

	set ips(i) {
		this[ips_] = i;
	}

	get ips() {
		return this[ips_];
	}

	set netns(ns) {
		assert(typeof ns === 'string' || ns === null);
		this[netns_] = ns;
	}

	get netns() {
		return this[netns_];
	}

	set port(port) {
		assert(typeof port === 'number' || port === null);
		this[port_] = port;
	}

	get port() {
		return this[port_];
	}

	// the subnets associated with the virtual interface of this instance
	// of tinc (tun0, etc). I.e., these will be served over the VPN.
	set subnets(s) { this[subnets_] = s; }
	get subnets() { return this[subnets_]; }

	set name(name) {
		assert(/^[_a-zA-Z0-9]+$/.test(name));
		this[name_] = name;
		// TODO: schedule restart
	}

	get name() {
		return this[name_];
	}

	get privateKey() {
		return this[privateKey_];
	}

	get publicKey() {
		return this[publicKey_];
	}

	async _writefiles(wait_signal) {
		assert(typeof wait_signal === 'string');
		await fs.emptyDir(this.configdir);
		await fs.ensureDir(path.join(this.configdir, 'hosts'));

		const files = [];

		files.push(fs.writeFile(path.join(this[configdir_], 'rsa_key.priv'), this.privateKey, {mode: 0o600}));

		let tinc_up = '#!/bin/sh\n';
		if (this.deviceType !== 'dummy') {
			let prefix = '';
			if (this.netns) {
				prefix = `ip netns exec ${this.netns} `;
				tinc_up += `ip link set $INTERFACE netns ${this.netns}\n`;
			}

			tinc_up += `${prefix}ip link set $INTERFACE up\n`;

			for (const ip of this.ips)
				tinc_up += `${prefix}ip address add ${ip} dev $INTERFACE\n`;

			for (const subnet of this.subnets)
				tinc_up += `${prefix}ip route add ${subnet} dev $INTERFACE\n`;
		}

		tinc_up += `echo '${wait_signal}' | nc -N ${server.address().address} ${server.address().port}\n`;
		files.push(fs.writeFile(
			path.join(this[configdir_], 'tinc-up'), tinc_up, {mode: 0o700}
		));

		let tinc_conf = `Name = ${this.name}\n`;
		for (const a of this.bindToAddress)
			tinc_conf += `BindToAddress = ${a}\n`;
		for (const c of this.connectTo)
			tinc_conf += `ConnectTo = ${c}\n`;
		tinc_conf += `Cipher = aes-256-cbc\n`;
		tinc_conf += `Digest = sha256\n`;
		if (this.deviceType)
			tinc_conf += `DeviceType = ${this.deviceType}\n`;
		if (this.port)
			tinc_conf += `Port = ${this.port}\n`;

		files.push(fs.writeFile(
			path.join(this[configdir_], 'tinc.conf'), tinc_conf
		));

		for (const host of this.hosts) {
			let host_file = `Name = ${host.name}\n`;
			if (host.addresses)
				for (const addr of host.addresses)
					host_file += `Address = ${addr}\n`;
			if (host.subnets)
				for (const subnet of host.subnets)
					host_file += `Subnet = ${subnet}\n`;
			if (host.publicKey)
				host_file += host.publicKey;
			files.push(fs.writeFile(path.join(this[configdir_], 'hosts', host.name), host_file));
		}

		// actually write files
		await Promise.all(files);
	}
});
