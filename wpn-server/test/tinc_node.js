'use strict';

const assert = require('assert');
const child_process = require('mz/child_process');
const fs = require('fs-extra');
const os = require('os');
const sleep = require('es7-sleep');
const tinc_node = require('../tinc_node');
const uuid = require('uuid-1345');

describe('tinc_node', function() {
	describe('constructor', function() {
		it('default name is random', async function() {
			const t1 = new tinc_node();
			const t2 = new tinc_node();
			assert.notEqual(t1.name, t2.name);
		});

		it('complex characters not allowed in name', async function() {
			try {
				const t = new tinc_node({name: 'a name'});
			} catch(e) {
				return;
			}
			assert(false);
		});
	});

	it('port setter sets port', async function() {
		const t = new tinc_node();
		t.port = 24601;
		assert.equal(24601, t.port);
	});

	describe('netns', function() {
		it('netns setter sets netns', async function() {
			const t = new tinc_node();
			t.netns = 'test';
			assert.equal('test', t.netns);
		});
	});

	describe('configdir', function() {
		it('configdir is random', async function() {
			const t1 = new tinc_node();
			const t2 = new tinc_node();
			assert.notEqual(t1.configdir, t2.configdir);
			assert.equal(typeof t1.configdir, 'string');
			assert.equal(typeof t2.configdir, 'string');
		});

		it('setter sets configdir', async function() {
			const t1 = new tinc_node();
			t1.configdir = '/tmp/dontuse';
			assert.equal('/tmp/dontuse', t1.configdir);
		});

		it('constructor sets configdir', async function() {
			const t1 = new tinc_node({configdir: '/tmp/what'});
			assert.equal(t1.configdir, '/tmp/what');
		});

		it('random configdir does not exist', async function() {
			const t1 = new tinc_node();
			try {
				await fs.access(t1.configdir);
			} catch(e) {
				return;
			}
			assert(false);
		});
	});

	describe('publicKey and privateKey', function() {
		this.timeout(10000);
		it('keys are strings', async function() {
			this.slow(500);
			const t1 = new tinc_node();
			await t1.generateKeyPair(512);
			assert.equal(typeof t1.privateKey, 'string');
			assert.equal(typeof t1.publicKey, 'string');
		});

		it('random keys are different', async function() {
			this.slow(500);
			const t1 = new tinc_node();
			const t2 = new tinc_node();
			await t1.generateKeyPair(512);
			await t2.generateKeyPair(512);
			assert.notEqual(t1.privateKey, t2.privateKey);
			assert.notEqual(t1.publicKey, t2.publicKey);
		});
	});

	describe('start and stop', function() {
		this.timeout(10000);
		it('start then stop does something', async function() {
			this.slow(1000);
			const t1 = new tinc_node();
			await t1.generateKeyPair(1024);
			await t1.start();
			await fs.access(t1.configdir);
			await t1.stop();
			try {
				await fs.access(t1.configdir);
			} catch(e) {
				return;
			}
			assert(false);
		});

		it('IP address exists', async function() {
			this.slow(2000);
			function ip_exists(ip_address) {
				const ni = os.networkInterfaces();
				let found = false;
				for (const e of Object.keys(ni))
					found = found || ni[e].some(ip => ip.address === ip_address);
				return found;
			}
			assert(!ip_exists('25.0.0.4'));
			const t1 = new tinc_node();
			t1.ips = ['25.0.0.4/24'];
			await t1.generateKeyPair(1024);
			await t1.start();

			assert(ip_exists('25.0.0.4'));

			await t1.stop();
		});

		it('Connection through two tinc nodes', async function() {
			this.slow(4000);
			const t1 = new tinc_node();
			const t2 = new tinc_node();
			try {
				await t1.generateKeyPair(1024);
				await t2.generateKeyPair(1024);
				t1.ips = ['25.0.0.1/24'];
				t2.ips = ['25.0.0.2/24'];
				t1.bindToAddress = ['127.0.0.2']
				t2.bindToAddress = ['127.0.0.3'];

				t1.hosts = [{
					name: t2.name,
					addresses: ['127.0.0.3'],
					subnets: ['25.0.0.2/32'],
					publicKey: t2.publicKey,
				},{
					name: t1.name,
					subnets: ['25.0.0.1/32'],
					publicKey: t1.publicKey,
				}];

				t2.hosts = [{
					name: t1.name,
					subnets: ['25.0.0.1/32'],
					publicKey: t1.publicKey,
				},{
					name: t2.name,
					subnets: ['25.0.0.2/32'],
					publicKey: t2.publicKey,
				}];

				t1.connectTo = [t2.name];

				t2.netns = 'wpn_t2_test_' + uuid.v4();
				await child_process.exec(`ip netns add ${t2.netns}`);
				await child_process.exec(`ip netns exec ${t2.netns} ip link set lo up`);

				await t2.start();
				await t1.start();
				await sleep(100);

				let listener = child_process.exec(`ip netns exec ${t2.netns} nc -l -p 3000`);
				listener.catch(e => console.log(e));
				await sleep(100);
				await child_process.exec(`echo 'Hello, world!' | nc -N 25.0.0.2 3000`);
				const res = await listener;

				assert.equal('Hello, world!\n', res[0]);
			} finally {
				await t1.stop();
				await t2.stop();

				await child_process.exec(`ip netns del ${t2.netns}`);
			}
		});
	});

	describe('deviceType', function() {
		it('set deviceType', async function() {
			const t1 = new tinc_node();
			t1.deviceType = 'dummy';
			assert.equal(t1.deviceType, 'dummy');
		});

		it('deviceType dummy starts up', async function() {
			this.slow(1000);
			const t1 = new tinc_node();
			t1.deviceType = 'dummy';
			await t1.generateKeyPair(1024);
			await t1.start();
			await t1.stop();
		});

		it('can ping through dummy', async function() {
			this.slow(2000);
			this.timeout(10000);

			// central 'dummy' node
			const ramona = new tinc_node();
			ramona.deviceType = 'dummy';
			ramona.bindToAddress = ['127.0.0.58'];
			ramona.name = 'ramona';

			// two client nodes
			const t1 = new tinc_node();
			t1.bindToAddress = ['127.0.0.59'];
			t1.ips = ['25.0.0.1/24'];
			t1.netns = 'dummy_t1_' + uuid.v4();
			t1.name = 't1';
			const t2 = new tinc_node();
			t2.bindToAddress = ['127.0.0.60'];
			t2.ips = ['25.0.0.2/24'];
			t2.netns = 'dummy_t2_' + uuid.v4();
			t2.name = 't2';

			await ramona.generateKeyPair(512);
			await t1.generateKeyPair(512);
			await t2.generateKeyPair(512);

			ramona.hosts = [{
				name: ramona.name,
				publicKey: ramona.publicKey,
			},{
				name: t1.name,
				publicKey: t1.publicKey,
			},{
				name: t2.name,
				publicKey: t2.publicKey,
			}];

			t1.hosts = [{
				name: ramona.name,
				publicKey: ramona.publicKey,
				addresses: ['127.0.0.58'],
			},{
				name: t1.name,
				publicKey: t1.publicKey,
				subnets: ['25.0.0.1/32'],
			}];

			t2.hosts = [{
				name: ramona.name,
				publicKey: ramona.publicKey,
				addresses: ['127.0.0.58'],
			},{
				name: t2.name,
				publicKey: t2.publicKey,
				subnets: ['25.0.0.2/32'],
			}];

			t1.connectTo = [ramona.name];
			t2.connectTo = [ramona.name];

			await child_process.exec(`ip netns add ${t1.netns}`);
			await child_process.exec(`ip netns add ${t2.netns}`);
			await child_process.exec(`ip netns exec ${t1.netns} ip link set lo up`);
			await child_process.exec(`ip netns exec ${t2.netns} ip link set lo up`);

			await ramona.start();
			await Promise.all([t1.start(), t2.start()]);

			try {
				await child_process.exec(`ip netns exec ${t1.netns} ping -c1 25.0.0.2`);
			} finally {
				await Promise.all([ramona.stop(), t1.stop(), t2.stop()]);
				await child_process.exec(`ip netns del ${t1.netns}`);
				await child_process.exec(`ip netns del ${t2.netns}`);
			}
		});
	});

	describe('subnets', function() {
		it('set subnets', async function() {
			const t1 = new tinc_node();
			t1.subnets = ['25.0.0.0/24'];
		});

		it('subnets starts up', async function() {
			this.slow(1000);
			const t1 = new tinc_node();
			t1.subnets = ['25.0.0.0/24'];
			await t1.generateKeyPair(1024);
			await t1.start();
			await t1.stop();
		});

		it('can ping with subnets', async function() {
			this.slow(2000);
			this.timeout(10000);

			const t1 = new tinc_node();
			t1.bindToAddress = ['127.0.0.59'];
			t1.ips = ['25.0.0.1/24'];
			t1.netns = 'dummy_t1_' + uuid.v4();
			const t2 = new tinc_node();
			t2.bindToAddress = ['127.0.0.60'];
			t2.ips = ['25.0.0.2/24', '25.2.2.2/32'];
			t2.netns = 'dummy_t2_' + uuid.v4();

			await t1.generateKeyPair(512);
			await t2.generateKeyPair(512);

			t1.hosts = [{
				name: t2.name,
				publicKey: t2.publicKey,
				addresses: ['127.0.0.60'],
			},{
				name: t1.name,
				publicKey: t1.publicKey,
				subnets: ['25.0.0.1/32'],
			}];

			t2.hosts = [{
				name: t1.name,
				publicKey: t1.publicKey,
			},{
				name: t2.name,
				publicKey: t2.publicKey,
				subnets: ['25.0.0.2/32'],
				subnets: ['25.2.2.2/32'],
			}];

			t1.subnets = ['25.2.2.2/32'];
			t1.connectTo = [t2.name];

			await child_process.exec(`ip netns add ${t1.netns}`);
			await child_process.exec(`ip netns add ${t2.netns}`);
			await child_process.exec(`ip netns exec ${t1.netns} ip link set lo up`);
			await child_process.exec(`ip netns exec ${t2.netns} ip link set lo up`);

			await t2.start();
			await t1.start();

			try {
				await child_process.exec(`ip netns exec ${t1.netns} ping -c1 25.2.2.2`);
			} finally {
				await Promise.all([t1.stop(), t2.stop()]);
				await child_process.exec(`ip netns del ${t1.netns}`);
				await child_process.exec(`ip netns del ${t2.netns}`);
			}
		});
	});
});
