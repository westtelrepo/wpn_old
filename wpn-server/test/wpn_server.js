'use strict';

const WebSocket = require('ws');
const assert = require('assert');
const uuid = require('uuid-1345');
const wpn_server = require('../wpn_server');

describe('wpn_server', function() {
	this.slow(1500);

	const database = new (require('./modules/database_setup'))();
	let db;
	let wpn;

	before(async function() {
		this.timeout(10000);
		await database.before();
	});

	beforeEach(async function() {
		this.timeout(10000);
		db = await require('../db')(await database.beforeEach());
		wpn = new wpn_server({db, port: 8080});
	});

	afterEach(async function() {
		this.timeout(10000);
		await wpn.close();
		await db.done();
		await database.afterEach();
	});

	after(async function() {
		await database.after();
	});

	it('empty', async function() {
	});

	it('PING', async function() {
		const ws = new WebSocket('ws://localhost:8080/');
		let got_pong;
		const wait_pong = new Promise(resolve => got_pong = resolve);

		ws.on('open', () => {
			ws.send(JSON.stringify({_c:'PING',_r:5}));
		});

		ws.on('message', data => {
			const m = JSON.parse(data);
			assert(m._c === 'OK');
			assert(m._r === -5);
			got_pong();
		});

		await wait_pong;
	});

	describe('LOGIN', function() {
		it('login', async function() {
			const uid = await db.transact(async t => {
				const id = await t.user_create({username: 'Ramona'});
				await t.user_set_password(id, 'password');
				return id;
			});

			let got_ok;
			const wait_ok = new Promise(resolve => got_ok = resolve);

			const ws = new WebSocket('ws://localhost:8080/');
			ws.on('open', () => {
				ws.send(JSON.stringify({_c: 'LOGIN', _r: 1, method: 'password', password: 'password', uid}));
			});

			ws.on('message', data => {
				const m = JSON.parse(data);
				assert(m._c === 'OK');
				assert(m._r === -1);
				got_ok();
			});

			await wait_ok;
		});

		it('login wrong password', async function() {
			const uid = await db.transact(async t => {
				const id = await t.user_create({username: 'Ramona'});
				await t.user_set_password(id, 'password');
				return id;
			});

			let got_ok;
			const wait_ok = new Promise(resolve => got_ok = resolve);

			const ws = new WebSocket('ws://localhost:8080/');
			ws.on('open', () => {
				ws.send(JSON.stringify({_c: 'LOGIN', _r: 10, method: 'password', password: 'passw0rd', uid}));
			});

			ws.on('message', data => {
				const m = JSON.parse(data);
				assert(m._c === 'ERROR');
				assert(m._r === -10);
				got_ok();
			});

			await wait_ok;
		});

		it('login wrong uid', async function() {
			const uid = await db.transact(async t => {
				const id = await t.user_create({username: 'Ramona'});
				await t.user_set_password(id, 'password');
				return id;
			});

			let got_ok;
			const wait_ok = new Promise(resolve => got_ok = resolve);

			const ws = new WebSocket('ws://localhost:8080/');
			ws.on('open', () => {
				ws.send(JSON.stringify({_c: 'LOGIN', _r: 10, method: 'password', password: 'password', uid: uuid.v4()}));
			});

			ws.on('message', data => {
				const m = JSON.parse(data);
				assert(m._c === 'ERROR');
				assert(m._r === -10);
				got_ok();
			});

			await wait_ok;
		});

		it('login with username', async function() {
			const uid = await db.transact(async t => {
				const id = await t.user_create({username: 'Ramona'});
				await t.user_set_password(id, 'password');
			});

			let got_ok;
			const wait_ok = new Promise(resolve => got_ok = resolve);

			const ws = new WebSocket('ws://localhost:8080/');
			ws.on('open', () => {
				ws.send(JSON.stringify({_c: 'LOGIN', _r: 1, method: 'password', password: 'password', username: 'Ramona'}));
			});

			ws.on('message', data => {
				const m = JSON.parse(data);
				assert(m._c === 'OK');
				assert(m._r === -1);
				got_ok();
			});

			await wait_ok;
		});

		it('login with username wrong password', async function() {
			await db.transact(async t => {
				const id = await t.user_create({username: 'Ramona'});
				await t.user_set_password(id, 'password');
			});

			let got_ok;
			const wait_ok = new Promise(resolve => got_ok = resolve);

			const ws = new WebSocket('ws://localhost:8080/');
			ws.on('open', () => {
				ws.send(JSON.stringify({_c: 'LOGIN', _r: 15, method: 'password', password: 'passw0rd', username: 'Ramona'}));
			});

			ws.on('message', data => {
				const m = JSON.parse(data);
				assert(m._c === 'ERROR');
				assert(m._r === -15);
				got_ok();
			});

			await wait_ok;
		});

		it('login wrong username', async function() {
			const uid = await db.transact(async t => {
				const id = await t.user_create({username: 'Ramona'});
				await t.user_set_password(id, 'password');
				return id;
			});

			let got_ok;
			const wait_ok = new Promise(resolve => got_ok = resolve);

			const ws = new WebSocket('ws://localhost:8080/');
			ws.on('open', () => {
				ws.send(JSON.stringify({_c: 'LOGIN', _r: 20, method: 'password', password: 'password', username: 'NotRamona'}));
			});

			ws.on('message', data => {
				const m = JSON.parse(data);
				assert(m._c === 'ERROR');
				assert(m._r === -20);
				got_ok();
			});

			await wait_ok;
		});
	});

	describe('WHOAMI', function() {
		it('simple', async function() {
			const uid = await db.transact(async t => {
				const id = await t.user_create({username: 'Ramona'});
				await t.user_set_password(id, 'password');
				return id;
			});

			let got_ok;
			const wait_ok = new Promise(resolve => got_ok = resolve);

			const ws = new WebSocket('ws://localhost:8080/');
			ws.on('open', () => {
				ws.send(JSON.stringify({_c: 'LOGIN', _r: 20, method: 'password', password: 'password', uid}));
			});

			ws.on('message', data => {
				const m = JSON.parse(data);
				if (m._r === -20) {
					ws.send(JSON.stringify({_c: 'WHOAMI', _r: 15}));
				} else if (m._r === -15) {
					assert.equal(m._c, 'OK');
					assert.equal(m.uid, uid);
					got_ok();
				}
			});

			await wait_ok;
		});
	});

	describe('CONNECT', function() {
		const rsa = '-----BEGIN PUBLIC KEY-----\nMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIPpA4f3LBjvZC4kGu4CeMpRU2AURXyU\nO9teCS7cs2sNgAiTZZvJ1yIjG0T2fMSipavQhuYgKLccLEG1z4B3k+8CAwEAAQ==\n-----END PUBLIC KEY-----\n';
		it('simple', async function() {
			this.timeout(10000);
			let network_id;
			let role_id;
			let user_id;
			await db.transact(async t => {
				network_id = await t.network_create({name: 'network'});
				role_id = await t.role_create({name: 'role', network_id});
				await t.role_assign_ip_range(role_id, {ip_begin: '10.0.0.1', ip_end: '10.0.0.1'});
				user_id = await t.user_create({username: 'user'});
				await t.user_set_password(user_id, 'password');
				await t.user_give_role(user_id, role_id);
			});

			let got_ok;
			const wait_ok = new Promise(resolve => got_ok = resolve);

			const ws = new WebSocket('ws://localhost:8080/');
			ws.on('open', () => {
				ws.send(JSON.stringify({_c: 'LOGIN', uid: user_id, _r: 1, method: 'password', password: 'password'}));
			});

			ws.on('message', data => {
				const m = JSON.parse(data);
				if (m._r === -1) {
					ws.send(JSON.stringify({_c: 'CONNECT', role_id: role_id, _r: 2, rsa_public_key: rsa}));
				} else if (m._r === -2) {
					assert(uuid.check(m.node_id));
					assert.equal(m.ips.length, 1);
					assert.equal(m.ips[0], '10.0.0.1/32');
					got_ok();
				}
			});

			await wait_ok;
		});

		it('CONNECT without authentication is error', async function() {
			const ws = new WebSocket('ws://localhost:8080/');
			ws.on('open', () => {
				ws.send(JSON.stringify({_c: 'CONNECT', role_id: uuid.v4(), _r: 2, rsa_public_key: rsa}));
			});

			let got_ok;
			const wait_ok = new Promise(resolve => got_ok = resolve);

			ws.on('message', data => {
				const m = JSON.parse(data);
				assert.equal(m._c, 'ERROR');
				assert.equal(m._r, -2);
				assert.equal(m.e, 'EACCESS');
				got_ok();
			});

			await wait_ok;
		});

		it('CONNECT without authorization is error', async function() {
			this.timeout(10000);
			let network_id;
			let role_id;
			let user_id;
			await db.transact(async t => {
				network_id = await t.network_create({name: 'network'});
				role_id = await t.role_create({name: 'role', network_id});
				await t.role_assign_ip_range(role_id, {ip_begin: '10.0.0.1', ip_end: '10.0.0.1'});
				user_id = await t.user_create({username: 'user'});
				await t.user_set_password(user_id, 'password');
			});

			let got_ok;
			const wait_ok = new Promise(resolve => got_ok = resolve);

			const ws = new WebSocket('ws://localhost:8080/');
			ws.on('open', () => {
				ws.send(JSON.stringify({_c: 'LOGIN', uid: user_id, _r: 1, method: 'password', password: 'password'}));
			});

			ws.on('message', data => {
				const m = JSON.parse(data);
				if (m._r === -1) {
					ws.send(JSON.stringify({_c: 'CONNECT', role_id: role_id, _r: 2, rsa_public_key: rsa}));
				} else if (m._r === -2) {
					assert.equal(m._c, 'ERROR');
					assert.equal(m.e, 'EACCESS');
					got_ok();
				}
			});

			await wait_ok;
		});
	});

	describe('LIST_MY_TINC', function() {
		it('simple', async function() {
			let user_id;
			let network_id;
			await db.transact(async t => {
				network_id = await t.network_create({name: 'network'});
				await t.tinc_assign_static({network_id, hostname: 'example.com', port: 50});
				user_id = await t.user_create({username: 'user'});
				await t.user_set_password(user_id, 'password');
				await t.user_give_hostname(user_id, 'example.com');
			});

			let got_ok;
			const wait_ok = new Promise(resolve => got_ok = resolve);

			const ws = new WebSocket('ws://localhost:8080/');
			ws.on('open', () => {
				ws.send(JSON.stringify({_c:'LOGIN', _r:1, uid: user_id, method: 'password', password: 'password'}));
			});
			ws.on('message', data => {
				const m = JSON.parse(data);
				if (m._r === -1) {
					assert.equal(m._c, 'OK');
					ws.send(JSON.stringify({_c:'LIST_MY_TINC', _r: 2}));
				} else if (m._r === -2) {
					assert.equal(m._c, 'OK');
					assert.equal(m.list.length, 1);
					assert.equal(m.list[0].network_id, network_id);
					assert.equal(m.list[0].hostname, 'example.com');
					assert.equal(m.list[0].static, true);
					assert.equal(m.list[0].rsa_public_key, null);
					got_ok();
				}
			});

			await wait_ok;
		});
	});
});
