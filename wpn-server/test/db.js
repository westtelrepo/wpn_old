'use strict';
const assert = require('assert');
const notp = require('notp');
const url = require('url');
const uuid = require('uuid-1345');

const database = new (require('./modules/database_setup'))();

// helpful constant, a random RSA public key
const rsa =
`-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxl+XCCN87QaaLlMTDw/h
tarNmKLxKkeLTcSBNAmEinbDWNQlwth3X/gUVFlkisbpUOXGDSVd0CMRSwHnnE7Y
MtCv2eQFDU5Da7Bwcw36NE6E/1Jqr9vhwzlhtIVO3TlCtx1jikZeRs4hB2vszZgM
sksqdMTK02GIJucEIiZtANepht2qJY/IldztlFyp+PZyU/J4CbGw0tAxtNsgdWdy
yKVo19jXOmtKKbCky8h/OFExHBdSi3nuaqTb/LQaCOzbQqQLXpg9YUtwFkjVSJJh
xvhQuFzOgI9pH3snY2dxN5SRNCN0c/5yFz7BbR/4KdmEzVjOCoe/9jKY1Bdp0f4n
fQIDAQAB
-----END PUBLIC KEY-----
`;

const rsa2 =
`-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAjHe9PKwbKGtW4lSq8FnP
hb3AZTYMYcNcjnbItY9CYLBkSyigN/4flhOGaJxmv/Ug4YVl0z8OSPeVmqlxeoG6
URCHZQXLdamCDFE51MOTaULBzcWw6EjqfnnDFCWIW8MHG/gnTEMDwMyDzVeKUWcs
x84/H2fTOQGhxpUL6iNI+p5UKVtHO/vvVBS4U7x89A7O7EH5qkOESkf9U9+lF+qr
NRKq7A/dCkNjlslG8i89sJEaQEvIwK63umK9EPXzmM9nBYAS4qszgltllIP11jqu
3VxzOavv37TQsQjEnhLl54FM9gtgatv67b1TQm7k5wTfArsOTZ6EXMrLCFxWU512
NqEQuVv9Zmf5Tv+XDLamTO9Ll8+Ahw13ObX5m0rODSBSIiOcdC0cBQBgAy9sF1X6
OtlzS3axX0vjLvd2MEHYxLcT2WZIrFZR8TfyOIQDVreWXFMHuCZFWNW+b33+DQu2
mwrlOHhLt+VBg4y7Sro86t1R+zXpvDUkXVuk9XnJxsN3DJqikMSBzlLKSRsMQmBo
QSqjyYw7SxrrQ0NvvH31q7FshKHOWgkd+G8Uu8vpjuuvBns5CVxgQgCPUbC2CnaX
8F2qppE59ePdR7luw7Iqg1rgvE+kSQLJMErlWfF1JRPuyRgx3/AJ73q9ysFNCpCD
4OJxe1Tg1g8xqBKNe0pf4z8CAwEAAQ==
-----END PUBLIC KEY-----
`;

describe('db', function() {
	let db;

	before(async function() {
		this.timeout(10000);
		await database.before();
	});

	beforeEach(async function() {
		this.timeout(10000);
		db = await require('../db')(await database.beforeEach());
	});

	afterEach(async function() {
		this.timeout(10000);
		await db.done();
		await database.afterEach();
	});

	after(async function() {
		await database.after();
	});

	describe('.transact()', function() {
		it('returns value', async function() {
			const value = await db.transact(async () => 'value');
			assert.equal(value, 'value');
		});
	});

	describe('.transactRO()', function() {
		it('returns value', async function() {
			const value = await db.transactRO(async () => 'value');
			assert.equal(value, 'value');
		});
	});

	describe('Transaction', function() {
		describe('.network_create()', function() {
			it('simple', async function() {
				const id = await db.transact(async t => await t.network_create({name: 'hello'}));
				assert(uuid.check(id));
			});

			it('two networks same name is exception', async function() {
				await db.transact(async t => {
					await t.network_create({name: 'hello'});
					try {
						await t.network_create({name: 'hello'});
					} catch(e) {
						return;
					}
					assert(false);
				});
			});

			it('name argument required', async function() {
				try {
					await db.transact(async t => t.network_create({}));
				} catch(e) {
					return;
				}
				assert(false);
			});
		});

		describe('.network_get_name()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.network_create({name: 'name'});
					const name = await t.network_get_name(id);
					assert.equal(name, 'name');
					assert.equal(typeof name, 'string');
				});
			});

			it('exception if does not exist', async function() {
				try {
					await db.transact(async t => await t.network_get_name('235e49d5-56fc-4416-915e-64bdc787eb0f'));
				} catch(e) {
					return;
				}
				assert(false);
			});
		});

		describe('.network_set_name()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.network_create({name: 'network'});
					assert.equal('network', await t.network_get_name(id));
					await t.network_set_name(id, 'Ramona');
					assert.equal('Ramona', await t.network_get_name(id));
				});
			});

			it('nonexistent networks can not have name set', async function() {
				await db.transact(async t => {
					try {
						await t.network_set_name(uuid.v4(), 'Ramona');
					} catch(e) {
						return;
					}
					assert(false);
				});
			});
		});

		describe('.network_exists()', function() {
			it('does not exist', async function() {
				const ret = await db.transact(async t => await t.network_exists(uuid.v4()));
				assert(!ret);
				assert.equal(typeof ret, 'boolean');
			});

			it('does exist', async function() {
				await db.transact(async t => {
					const id = await t.network_create({name:'name'});
					const exists = await t.network_exists(id);
					assert(exists);
					assert.equal(typeof exists, 'boolean');
				});
			});

			it('one does and one no longer', async function() {
				this.slow(200);
				await db.transact(async t => {
					const id1 = await t.network_create({name:'network1'});
					const id2 = await t.network_create({name:'network2'});
					assert(await t.network_exists(id1));
					assert(await t.network_exists(id2));
					await t.network_delete(id1);
					assert(!await t.network_exists(id1));
					assert(await t.network_exists(id2));
				});
			});
		});

		describe('.network_delete()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.network_create({name:'name'});
					await t.network_delete(id);
					assert(!await t.network_exists(id));
				});
			});

			it('delete non-existing network is not an error', async function() {
				await db.transact(async t => t.network_delete(uuid.v4()));
			});
		});

		describe('.tinc_assign_static()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.network_create({name:'name'});
					await t.tinc_assign_static({network_id: id, hostname: 'localhost', port: 5000})
				});
			});

			it('no overlapping hostname and ports', async function() {
				await db.transact(async t => {
					const id1 = await t.network_create({name:'name1'});
					const id2 = await t.network_create({name:'name2'});
					await t.tinc_assign_static({network_id: id1, hostname: 'localhost', port: 5000});
					try {
						await t.tinc_assign_static({network_id: id2, hostname: 'localhost', port: 5000});
					} catch(e) {
						return;
					}
					assert(false);
				});
			});
		});

		describe('.tinc_get_servers()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.network_create({name: 'name'});
					await t.tinc_assign_static({network_id: id, hostname: 'localhost', port: 5000});
					const ret = await t.tinc_get_servers(id);
					assert.equal(ret.length, 1);
					assert.equal(ret[0].hostname, 'localhost');
					assert.equal(ret[0].port, 5000);
					assert.equal(ret[0].network_id, id);
					assert.equal(ret[0].static, true);
					assert.equal(ret[0].rsa_public_key, null);
				});
			});

			it('only get the network asked for', async function() {
				await db.transact(async t => {
					const id1 = await t.network_create({name: 'Ramona'});
					const id2 = await t.network_create({name: 'JeanValjean'});
					await t.tinc_assign_static({network_id: id1, hostname: 'roam.westtel.com', port: 42});
					await t.tinc_assign_static({network_id: id2, hostname: 'mynameis.example', port: 24601});
					const ret = await t.tinc_get_servers(id1);
					assert.equal(ret.length, 1);
					assert.equal(ret[0].hostname, 'roam.westtel.com');
				});
			});
		});

		describe('.tinc_get_servers_by_hostname()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.network_create({name: 'name'});
					await t.tinc_assign_static({network_id: id, hostname: 'localhost', port: 5000});
					const ret = await t.tinc_get_servers_by_hostname('localhost');
					assert.equal(ret.length, 1);
					assert.equal(ret[0].port, 5000);
				});
			});
		});

		describe('.tinc_get_servers_by_userid()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.network_create({name: 'name'});
					const uid = await t.user_create({username: 'user'});
					await t.user_give_hostname(uid, 'example.com');
					await t.tinc_assign_static({network_id: id, hostname: 'example.com', port: 50});
					const list = await t.tinc_get_servers_by_userid(uid);

					assert.equal(list.length, 1);
					assert.equal(list[0].hostname, 'example.com');
					assert.equal(list[0].network_id, id);
					assert.equal(list[0].port, 50);
					assert.equal(list[0].rsa_public_key, null);
					assert.equal(list[0].static, true);
				});
			});

			it('only get hostnames owned', async function() {
				await db.transact(async t => {
					const network_id = await t.network_create({name: 'name'});
					const uid = await t.user_create({username: 'user'});
					await t.user_give_hostname(uid, 'a.example.com');
					await t.tinc_assign_static({network_id, hostname: 'a.example.com', port: 60});
					await t.tinc_assign_static({network_id, hostname: 'b.example.com', port: 70});

					const list = await t.tinc_get_servers_by_userid(uid);
					assert.equal(list.length, 1);
					assert.equal(list[0].hostname, 'a.example.com');
					assert.equal(list[0].network_id, network_id);
					assert.equal(list[0].port, 60);
					assert.equal(list[0].rsa_public_key, null);
					assert.equal(list[0].static, true);
				});
			});

			it('nonexistent user has no servers', async function() {
				await db.transactRO(async t => {
					const list = await t.tinc_get_servers_by_userid(uuid.v4());
					assert.equal(list.length, 0);
				});
			});
		});

		describe('.tinc_update()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.network_create({name: 'name'});
					await t.tinc_assign_static({network_id: id, hostname: 'localhost', port: 24601});
					await t.tinc_update({network_id: id, hostname: 'localhost', port: 24601}, {rsa_public_key: rsa});
					const result = await t.tinc_get_servers(id);
					assert.equal(result.length, 1);
					assert.equal(result[0].rsa_public_key, rsa);
				});
			});

			it('only updates one', async function() {
				await db.transact(async t => {
					const id = await t.network_create({name: 'name'});
					await t.tinc_assign_static({network_id: id, hostname: 'localhost', port: 24601});
					await t.tinc_assign_static({network_id: id, hostname: 'localhost', port: 42});
					await t.tinc_update({network_id: id, hostname: 'localhost', port: 24601}, {rsa_public_key: rsa});
					const result = await t.tinc_get_servers(id);
					// sort only by port, everything else is the same
					result.sort((a, b) => a.port - b.port);
					assert.equal(result.length, 2);
					assert.equal(result[0].rsa_public_key, null);
					assert.equal(result[1].rsa_public_key, rsa);
				});
			});
		});

		describe('.user_create()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.user_create({username:'Ramona'});
					assert(uuid.check(id));
				});
			});

			it('two users can not have the same name', async function() {
				await db.transact(async t => {
					await t.user_create({username:'spartacus'});
					try {
						await t.user_create({username:'spartacus'});
					} catch(e) {
						return;
					}
					assert(false);
				});
			});
		});

		describe('.user_get_name()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.user_create({username:'username'});
					const name = await t.user_get_name(id);
					assert.equal(name, 'username');
				});
			});

			it('return right username', async function() {
				await db.transact(async t => {
					const id = await t.user_create({username: 'spartacus_1'});
					await t.user_create({username: 'spartacus_2'});
					const name = await t.user_get_name(id);
					assert.equal(name, 'spartacus_1');
				});
			});

			it('get username of nonexistent user is an error', async function() {
				await db.transact(async t => {
					const id = await t.user_create({username: 'that_guy'});
					try {
						await t.user_get_name(uuid.v4());
					} catch(e) {
						return;
					}
					// if you get here by chance go buy a lottery ticket
					assert(false);
				});
			});
		});

		describe('.user_get_uid_from_username()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.user_create({username: 'Ramona'});
					assert.equal(id, await t.user_get_uid_from_username('Ramona'));
				});
			});

			it('get the right one', async function() {
				await db.transact(async t => {
					const id1 = await t.user_create({username: 'Ramona'});
					const id2 = await t.user_create({username: 'NotRamona'});
					assert.equal(id1, await t.user_get_uid_from_username('Ramona'));
				});
			});

			it('nonexistant username has no uid', async function() {
				await db.transactRO(async t => {
					assert.equal(null, await t.user_get_uid_from_username('unobtanium'));
				});
			});

			it('nonexistant username has no uid even if other users exist', async function() {
				await db.transact(async t => {
					await t.user_create({username: 'obtanium'});
					assert.equal(null, await t.user_get_uid_from_username('unobtanium'));
				});
			});
		});

		describe('.user_set_password()', function() {
			it('simple', async function() {
				this.slow(750);
				await db.transact(async t => {
					const id = await t.user_create({username: 'Shirley'});
					await t.user_set_password(id, 'pa$$w0rd');
				});
			});

			it('nonexistent users can\'t have passwords', async function() {
				this.slow(750);
				await db.transact(async t => {
					try {
						await t.user_set_password(uuid.v4(), 'what');
					} catch(e) {
						return;
					}
					assert(false);
				});
			});
		});

		describe('.user_validate_password()', function() {
			it('simple', async function() {
				this.slow(1500);
				await db.transact(async t => {
					const id = await t.user_create({username: 'fun'});
					await t.user_set_password(id, 'password');
					assert(await t.user_validate_password(id, 'password'));
				});
			});

			it('wrong password does not validate', async function() {
				this.slow(1500);
				await db.transact(async t => {
					const id = await t.user_create({username: 'username'});
					await t.user_set_password(id, 'right password');
					assert(!await t.user_validate_password(id, 'wrong password'));
				});
			});

			it('password does not validate if user has no password set', async function() {
				this.slow(1000);
				await db.transact(async t => {
					const id = await t.user_create({username: 'username'});
					assert(!await t.user_validate_password(id, ''));
				});
			});

			it('password does not validate for nonexisting user', async function() {
				this.slow(1000);
				await db.transactRO(async t => {
					assert(!await t.user_validate_password(uuid.v4(), 'password'));
				});
			});

			it('null uid does not validate', async function() {
				this.slow(1000);
				await db.transactRO(async t => {
					assert(!await t.user_validate_password(null, 'passw0rd'));
				});
			});
		});

		describe('.user_totp_generate_key()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.user_create({username: 'username'});
					const key = await t.user_totp_generate_key(id);
					assert.equal(typeof key, 'string');
				});
			});
		});

		describe('.user_totp_verify_token()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const id = await t.user_create({username: 'username'});
					const key = await t.user_totp_generate_key(id);
					const token = notp.totp.gen(key);
					assert(await t.user_totp_verify_token(id, token));
				});
			});

			it('wrong token fails', async function() {
				await db.transact(async t => {
					const id = await t.user_create({username: 'username'});
					const key = await t.user_totp_generate_key(id);
					const token = notp.hotp.gen(key);
					// we replace the first character with a different digit so that
					// we have a `valid length' token that is highly likely to be invalid
					// (about window size * 1 / 10^6 chance of being valid)
					const replace = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
					const bad_token = replace[token[0]] + token.substr(1);
					assert.equal(token.length, bad_token.length);
					assert.notEqual(token, bad_token);
					assert(!await t.user_totp_verify_token(id, bad_token));
				});
			});

			it('token does not verify when no secret', async function() {
				await db.transact(async t => {
					const id = await t.user_create({username: 'username'});
					assert(!await t.user_totp_verify_token(id, ''));
				});
			});
		});

		describe('.user_has_role()', function() {
			it('nonexistent user does not have nonexistent role', async function() {
				await db.transact(async t => {
					assert(!await t.user_has_role(uuid.v4(), uuid.v4()));
				});
			});
		});

		describe('.user_give_role()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const uid = await t.user_create({username: 'user'});
					const nid = await t.network_create({name: 'network'});
					const rid = await t.role_create({name: 'role', network_id: nid});
					assert(!await t.user_has_role(uid, rid));
					await t.user_give_role(uid, rid);
					assert(await t.user_has_role(uid, rid));
				});
			});

			it('idempotent', async function() {
				await db.transact(async t => {
					const uid = await t.user_create({username: 'user'});
					const nid = await t.network_create({name: 'network'});
					const rid = await t.role_create({name: 'role', network_id: nid});
					assert(!await t.user_has_role(uid, rid));
					await t.user_give_role(uid, rid);
					assert(await t.user_has_role(uid, rid));
					await t.user_give_role(uid, rid);
					assert(await t.user_has_role(uid, rid));
				});
			});

			it('user does not have access to nonexistent role', async function() {
				await db.transact(async t => {
					const uid = await t.user_create({username: 'user'});
					const network_id = await t.network_create({name: 'network'});
					const rid = await t.role_create({name: 'role', network_id});
					await t.user_give_role(uid, rid);
					assert(!await t.user_has_role(uid, uuid.v4()));
				});
			});

			it('nonexistent user does not have access to role', async function() {
				await db.transact(async t => {
					const uid = await t.user_create({username: 'user'});
					const network_id = await t.network_create({name: 'network'});
					const rid = await t.role_create({name: 'role', network_id});
					await t.user_give_role(uid, rid);
					assert(!await t.user_has_role(uuid.v4(), rid));
				});
			});
		});

		describe('.user_has_hostname()', function() {
			it('nonexistent user does not have nonexistent hostname', async function() {
				await db.transact(async t => {
					assert(!await t.user_has_hostname(uuid.v4(), 'ramona.example'));
				});
			});
		});

		describe('.user_give_hostname()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const uid = await t.user_create({username: 'user'});
					assert(!await t.user_has_hostname(uid, 'example.com'));
					await t.user_give_hostname(uid, 'example.com');
					assert(await t.user_has_hostname(uid, 'example.com'));
				});
			});

			it('idempotent', async function() {
				await db.transact(async t => {
					const uid = await t.user_create({username: 'user'});
					assert(!await t.user_has_hostname(uid, 'example.com'));
					await t.user_give_hostname(uid, 'example.com');
					assert(await t.user_has_hostname(uid, 'example.com'));
					await t.user_give_hostname(uid, 'example.com');
					assert(await t.user_has_hostname(uid, 'example.com'));
				});
			});

			it('user does not have access to nonexistent hostname', async function() {
				await db.transact(async t => {
					const uid = await t.user_create({username: 'user'});
					await t.user_give_hostname(uid, 'example.com');
					assert(!await t.user_has_hostname(uid, 'not.example.com'));
				});
			});

			it('nonexistent user does not have access to hostname', async function() {
				await db.transact(async t => {
					const uid = await t.user_create({username: 'user'});
					await t.user_give_hostname(uid, 'example.com');
					assert(!await t.user_has_hostname(uuid.v4(), 'example.com'));
				});
			});
		});

		describe('.role_create()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const network = await t.network_create({name: 'network'});
					const role = await t.role_create({name: 'role', network_id: network});
					assert(uuid.check(role));
				});
			});

			it('cant add roles to nonexistent network', async function() {
				await db.transact(async t => {
					try {
						await t.role_create({name: 'role', network_id: uuid.v4()});
					} catch(e) {
						return;
					}
					assert(false);
				});
			});

			it('network cant have two roles with same name', async function() {
				await db.transact(async t => {
					const network = await t.network_create({name: 'network'});
					await t.role_create({name: 'spartacus', network_id: network});
					try {
						await t.role_create({name: 'spartacus', network_id: network});
					} catch(e) {
						return;
					}
					assert(false);
				});
			});

			it('two networks can have roles with same name', async function() {
				await db.transact(async t => {
					const n1 = await t.network_create({name: 'Rome'});
					const n2 = await t.network_create({name: 'Germany'});
					await t.role_create({name: 'spartacus', network_id: n1});
					await t.role_create({name: 'spartacus', network_id: n2});
				});
			});
		});

		describe('.role_assign_ip_range()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r = await t.role_create({name: 'role', network_id: n});
					await t.role_assign_ip_range(r, {ip_begin: '10.100.0.1', ip_end: '10.100.0.123', masklen: 24});
				});
			});

			it('cant assign same range multiple times even with different masklen', async function() {
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r = await t.role_create({name: 'role', network_id: n});
					await t.role_assign_ip_range(r, {ip_begin:'10.100.0.1', ip_end:'10.100.0.2', masklen: 30});
					try {
						await t.role_assign_ip_range(r, {
							ip_begin: '10.100.0.1', ip_end: '10.100.0.2', masklen: 24
						});
					} catch(e) {
						return;
					}
					assert(false);
				});
			});

			it('cant assign overlapping ranges to same role', async function() {
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r = await t.role_create({name: 'role', network_id: n});
					await t.role_assign_ip_range(r, {ip_begin: '10.100.0.1', ip_end: '10.100.0.127', masklen: 16});
					try {
						await t.role_assign_ip_range(r, {
							ip_begin: '10.100.0.40', ip_end: '10.100.0.253', masklen: 24
						});
					} catch(e) {
						return;
					}
					assert(false);
				});
			});
		});

		describe('.role_instantiate()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r = await t.role_create({name: 'role', network_id: n});
					await t.role_assign_ip_range(r, {ip_begin: '10.100.0.1', ip_end: '10.100.0.1', masklen: 24});
					const ret = await t.role_instantiate(r, {rsa_public_key: rsa});
					assert(uuid.check(ret.node_id));
				});
			});

			it('single IP address cant be given out twice', async function() {
				this.slow(200);
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r = await t.role_create({name: 'role', network_id: n});
					await t.role_assign_ip_range(r, {ip_begin: '10.100.0.1', ip_end: '10.100.0.1', masklen: 24});
					await t.role_instantiate(r, {rsa_public_key: rsa});
					try {
						await t.role_instantiate(r, {rsa_public_key: rsa});
					} catch(e) {
						return;
					}
					assert(false);
				});
			});

			it('two IP addresses can be given simultaneously', async function() {
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r = await t.role_create({name: 'role', network_id: n});
					await t.role_assign_ip_range(r, {ip_begin: '10.100.0.1', ip_end: '10.100.0.2', masklen: 24});
					const n1 = await t.role_instantiate(r, {rsa_public_key: rsa});
					const n2 = await t.role_instantiate(r, {rsa_public_key: rsa});
					assert.notEqual(n1.ips[0], n2.ips[0]);
				});
			});

			it('two IP addresses from two ranges', async function() {
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r = await t.role_create({name: 'role', network_id: n});
					await t.role_assign_ip_range(r, {ip_begin: '10.100.0.1', ip_end: '10.100.0.1', masklen: 24});
					await t.role_assign_ip_range(r, {ip_begin: '10.200.0.1', ip_end: '10.200.0.1', masklen: 24});
					const n1 = await t.role_instantiate(r, {rsa_public_key: rsa});
					const n2 = await t.role_instantiate(r, {rsa_public_key: rsa});
					assert.notEqual(n1.ips[0], n2.ips[0]);
				});
			});

			it('two roles have same range', async function() {
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r1 = await t.role_create({name: 'role1', network_id: n});
					const r2 = await t.role_create({name: 'role2', network_id: n});
					await t.role_assign_ip_range(r1, {ip_begin: '10.100.0.1', ip_end: '10.100.0.2', masklen: 24});
					await t.role_assign_ip_range(r2, {ip_begin: '10.100.0.1', ip_end: '10.100.0.2', masklen: 24});
					const n1 = await t.role_instantiate(r1, {rsa_public_key: rsa});
					const n2 = await t.role_instantiate(r2, {rsa_public_key: rsa});
					assert.notEqual(n1.ips[0], n2.ips[0]);
				});
			});

			it('three nodes on two roles and overlapping ranges', async function() {
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r1 = await t.role_create({name: 'role', network_id: n});
					const r2 = await t.role_create({name: 'role2', network_id: n});
					await t.role_assign_ip_range(r1, {ip_begin: '10.100.0.1', ip_end: '10.100.0.2', masklen: 23});
					await t.role_assign_ip_range(r2, {ip_begin: '10.100.0.2', ip_end: '10.100.0.3', masklen: 25});
					await t.role_instantiate(r1, {rsa_public_key: rsa});
					await t.role_instantiate(r1, {rsa_public_key: rsa});
					const n3 = await t.role_instantiate(r2, {rsa_public_key: rsa});
					assert.equal(n3.ips[0], '10.100.0.3/25');
				});
			});

			it('two networks can have overlapping IP addresses', async function() {
				await db.transact(async t => {
					const n1 = await t.network_create({name: 'network1'});
					const n2 = await t.network_create({name: 'network2'});
					const r1 = await t.role_create({name: 'role', network_id: n1});
					const r2 = await t.role_create({name: 'role', network_id: n2});
					await t.role_assign_ip_range(r1, {ip_begin: '10.100.0.1', ip_end: '10.100.0.1'});
					await t.role_assign_ip_range(r2, {ip_begin: '10.100.0.1', ip_end: '10.100.0.1'});
					const node1 = await t.role_instantiate(r1, {rsa_public_key: rsa});
					const node2 = await t.role_instantiate(r2, {rsa_public_key: rsa});
					assert.equal(node1.ips[0], node2.ips[0]);
				});
			});
		});

		describe('.node_get_info()', function() {
			it('rsa_public_key', async function() {
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r = await t.role_create({name: 'role', network_id: n});
					await t.role_assign_ip_range(r, {ip_begin: '10.100.0.1', ip_end: '10.100.0.1'});
					const node = await t.role_instantiate(r, {rsa_public_key: rsa});
					const info = await t.node_get_info(node.node_id);
					assert.equal(info.rsa_public_key, rsa);
				});
			});
		});

		describe('.node_update()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r = await t.role_create({name: 'role', network_id: n});
					await t.role_assign_ip_range(r, {ip_begin: '10.100.0.1', ip_end: '10.100.0.1'});
					const node = await t.role_instantiate(r, {rsa_public_key: rsa});
					await t.node_update(node.node_id, {rsa_public_key: rsa2});
					const info = await t.node_get_info(node.node_id);
					assert.equal(info.rsa_public_key, rsa2);
				});
			});

			it('update one of two nodes', async function() {
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r = await t.role_create({name: 'role', network_id: n});
					await t.role_assign_ip_range(r, {ip_begin: '10.100.0.1', ip_end: '10.100.0.2', masklen: 27});
					const node1 = await t.role_instantiate(r, {rsa_public_key: rsa});
					const node2 = await t.role_instantiate(r, {rsa_public_key: rsa});
					const info1_before = await t.node_get_info(node1.node_id);
					const info2_before = await t.node_get_info(node2.node_id);
					assert.equal(info1_before.rsa_public_key, rsa);
					assert.equal(info2_before.rsa_public_key, rsa);
					await t.node_update(node1.node_id, {rsa_public_key: rsa2});
					const info1 = await t.node_get_info(node1.node_id);
					const info2 = await t.node_get_info(node2.node_id);
					assert.equal(info1.rsa_public_key, rsa2);
					assert.equal(info2.rsa_public_key, rsa);
				});
			});
		});

		describe('.node_update_last_seen()', function() {
			it('simple', async function() {
				await db.transact(async t => {
					const n = await t.network_create({name: 'network'});
					const r = await t.role_create({name: 'role', network_id: n});
					await t.role_assign_ip_range(r, {ip_begin: '10.100.0.1', ip_end: '10.100.0.1'});
					const node = await t.role_instantiate(r, {rsa_public_key: rsa});
					await t.node_update_last_seen(node.node_id);
				});
			});
		});
	});

	describe('.listen_network_change()', async function() {
		it('simple', async function() {
			db.listen_network_change(uuid.v4(), data => {});
		});

		it('network name changes', async function() {
			const n = await db.transact(async t => await t.network_create({name: 'network'}));
			let notify;
			let wait = new Promise(resolve => notify = resolve);
			db.listen_network_change(n, notify);
			await db.transact(async t => t.network_set_name(n, 'Ramona'));
			await wait;
		});

		it('only one network change is notified', async function() {
			this.slow(500);
			const [n1, n2] = await Promise.all([
				await db.transact(async t => await t.network_create({name: 'network1'})),
				await db.transact(async t => await t.network_create({name: 'network2'}))
			]);
			let i = 0;
			db.listen_network_change(n1, () => ++i);
			await db.transact(async t => await t.network_set_name(n1, 'Ramona'));
			await new Promise(resolve => setTimeout(resolve, 100));
			assert.equal(i, 1);
		});

		it('network change is not notified if transaction rolls back', async function() {
			this.slow(500);
			const n = await db.transact(async t => await t.network_create({name: 'network'}));
			let i = 0;
			await new Promise(resolve => setTimeout(resolve, 100));
			db.listen_network_change(n, () => i++);
			try {
				await db.transact(async t => {
					await t.network_set_name(n, 'Ramona');
					throw new Error('error!');
				});
			} catch(e) {
			}
			await new Promise(resolve => setTimeout(resolve, 100));
			assert.equal(i, 0);
		});

		it('connection recovery', async function() {
			this.slow(750);
			const n = await db.transact(async t => await t.network_create({name: 'network'}));
			let i = 0;
			await new Promise(resolve => setTimeout(resolve, 100));
			db.listen_network_change(n, () => i++);
			await database.kill_connections();
			await new Promise(resolve => setTimeout(resolve, 100));
			await db.transact(async t => await t.network_set_name(n, 'Ramona'));
			await new Promise(resolve => setTimeout(resolve, 100));
			assert(i > 0);
		});
	});
});
