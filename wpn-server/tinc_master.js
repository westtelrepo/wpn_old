'use strict';

const TincNode = require('./tinc_node');

const slaves_ = Symbol('tinc_master.slaves_');

module.exports = Object.freeze(class {
	constructor() {
		// slaves, instances of TincNode
		this[slaves_] = new Map();
		Object.freeze(this);
	}

	async stop() {
		let death = [];
		for(const slave of this[slaves_].values())
			death.push(slave.stop());
		await Promise.all(death);
	}

	async update(network_id, {port = null, hosts = [], bits = 2048} = {}) {
		if (!this[slaves_].has(network_id)) {
			const t = new TincNode();
			t.deviceType = 'dummy';
			this[slaves_].set(network_id, t);
		}

		const t = this[slaves_].get(network_id);
		await Promise.all([t.stop(), t.generateKeyPair()]);
		t.port = port;
		t.hosts = hosts.slice().concat([{name: t.name, publicKey: t.publicKey}]);
		await t.start();
		return {publicKey: t.publicKey, name: t.name};
	}

	async delete(network_id) {
		if (this[slaves_].has(network_id)) {
			const t = this[slaves_].get(network_id);
			this[slaves_].delete(network_id);
			await t.stop();
		}
	}
});
